Pod::Spec.new do |s|
  s.name              = "DDKit"
  s.version           = "1.0.1"
  s.summary           = "DDKit provides common features for Dennis iOS magazines"
  s.author            = { "Axel Niklasson" => "axel_niklasson@dennis.co.uk" }
  s.homepage          = 'https://bitbucket.org/Dennis_Media_Factory/ddkit'
  s.license           = 'COMMERCIAL'
  s.platform          = :ios, '8.0'

  s.source            = { :git => 'https://bitbucket.org/Dennis_Media_Factory/ddkit.git', :tag => "#{s.version}" }
  s.source_files      = 'Pod/Source/**/*.{h,m}'
  non_arc_files       = 'Pod/Source/KeychainItemWrapper.{h,m}'
  s.ios.exclude_files = 'Source', non_arc_files
  
  s.requires_arc      = true
  
  s.subspec 'no-arc' do |sna|
    sna.requires_arc  = false
    sna.source_files  = non_arc_files
  end
  
  s.ios.vendored_frameworks = 'Frameworks/Pugpig.framework'
  s.frameworks = 'MessageUI'
  s.libraries = 'xml2'
  s.dependency 'Parse', '~> 1.7'
  s.dependency 'Reachability', '~> 3.2'
  s.dependency 'XMLDictionary', '~> 1.4'
end
